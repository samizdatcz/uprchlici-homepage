ig.createFaq = (container) ->
  questions = d3.tsv.parse ig.data.faq
  faq = container.append \div
    ..attr \class "col faq"
    ..append \h2 .html "Otázky a odpovědi"
    ..append \div
      ..attr \id \faq
      ..append \ul
        ..selectAll \li .data questions .enter!append \li
          ..append \a
            ..attr \href \#
            ..html (.question)
            ..on \click
          ..append \div
            ..attr \class \answer
            ..html ->
              "<h1>#{it.question}</h1>" + it.answer
          ..append \div
            ..attr \class \filler
