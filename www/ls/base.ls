d3.select \#wrapper-inner .remove!
container = d3.select \#wrapper .append \div
  ..attr \id \container

container.append \div
  ..attr \class \top-banner
    ..append \a
      ..attr \href \http://rozhl.as/cilnemecko
      ..append \img
        ..attr \src "//samizdat.cz/data/uprchlici-homepage/www/media/banner.png"
container
  ..append \div
    ..attr \class "col audiomapa"
      ..append \h2 .html "Přehrajte si exkluzivní reportáže"
      ..append \iframe
        ..attr \src \https://samizdat.cz/data/uprchlici-audiomapa/www/
        ..attr \width 770
        ..attr \height 650


ig.createFaq container

articleCol = container.append \div
  ..attr \class "col articles"
  ..append \h2 .html "Články o uprchlické krizi"
articles = articleCol.append \div
  ..attr \class "articles-container box box-rubrika"

ig.createArticles articles
