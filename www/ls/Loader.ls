class ig.Loader
  ->
    @element = d3.select document.createElement \div
      ..attr \class \loader
    @bgElement = @element.append \div
      ..attr \class "element bg"
    @fgElement = @element.append \div
      ..attr \class "element fg"

  remove: ->
    @clearInterval!
    @element.remove!
    @stage = 0

  attach: (@parentElement) ->
    @parentElement.node!appendChild @element.node!
    @setInterval!

  setInterval: ->
    return if @interval
    @interval = setInterval do
      @~tick
      750
    @stage = -1
    @tick!

  tick: ->
    @stage++
    @stage %%= 9
    @fgElement.attr \class "element fg stage-#{@stage}"

  clearInterval: ->
    return unless @interval
    clearInterval @interval
    @interval = null
