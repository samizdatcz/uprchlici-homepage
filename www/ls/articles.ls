paging = 10
position = 0
loadingNext = no
reachedBottom = no
$body = $ "body"
$window = $ window
ig.createArticles = (container) ->
  html = ""
  element = container.append \div
  loaderContainer = container.append \div
    ..attr \class \loader-container
  loader = new ig.Loader
  next = ->
    loader.attach loaderContainer
    (err, data) <~ loadNext!
    loader.remove!
    html += data
    element.html html
  next!
  $window.bind \scroll ->
    pageHeight = $body.height!
    windowHeight = $ window .height!
    scrollTop = $body.scrollTop!
    scrollBottom = scrollTop + windowHeight
    remainingPixels = pageHeight - scrollBottom
    if remainingPixels < 200
      next!

loadNext = (cb) ->
  return if loadingNext or reachedBottom
  loadingNext := yes
  (err, data) <~ d3.text "https://samizdat.cz/proxy/rozhlas/zpravy/zahranici/_themeline/2632?pos=#{position}&mode=10"
  loadingNext := false
  if err
    cb err
    return
  $data = $ data
    ..find '.lista_nav' .detach!

  if $data.find "li.item" .length < 10
    reachedBottom := yes
  position += paging
  cb do
    null
    $data.find '#box-topic-1 .content .column-1' .html!

